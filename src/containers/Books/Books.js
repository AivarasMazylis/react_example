import React, { Component } from 'react';
import axios from 'axios';
import BooksList from '../../components/BooksList';

class Books extends Component {
  constructor(props) {
    super(props);

    this.deleteBook = this.deleteBook.bind(this);

    this.state = {
      books: [],
      error: undefined
    }
  }

  componentDidMount() {
    this.fetchBooks();
  }

  fetchBooks() {
    axios
    .get('https://sample-node-exercise.herokuapp.com/authors')
    .then(books => {
      this.setState(state => ({
        books: books.data,
        error: undefined
      }))
    })
    .catch(error => console.log(error));
  }

 deleteBook(bookId) {
  axios
  .delete(`https://sample-node-exercise.herokuapp.com/authors/${bookId}`)
  .then(books => {
   this.setState(state => ({
    books: state.books.filter(book => book.id !== bookId),
    error: undefined
  }));
  })
  .catch(error => {
    this.setState(state => ({
      books: state.books,
      error: error.message,
    }));
  });
 }

 addBook(event) {
  event.preventDefault();

  if (!this.state.text.length) {
    return;
  }

  const newBook = {
    BirthDate: this.state.BirthDate,
    Name: this.state.Name,
    Surname: this.state.Surname,
    stillALive: this.state.stillALive
  };

  axios.post(`https://sample-node-exercise.herokuapp.com/authors`, { newBook })
  .then(res => {
    console.log(res);
    console.log(res.data);
  })

 }



    render() {
      return (
        <div className="App">
            <h1>Books</h1>
            <BooksList books={this.state.books} deleteBook={this.deleteBook}/>
            </div>
      );
    }
  }

  export default Books;