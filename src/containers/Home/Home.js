import React, { Component } from 'react';
import HelloMessage from '../../components/HelloMessage';

class Home extends Component {
    render() {
      return (
        <div className="App">
            <HelloMessage name = "Aivaras"/>
        </div>
      );
    }
  }

  export default Home;