import React, { Component } from 'react';
import { Button } from 'reactstrap';
import './Authors.css';

class Authors extends Component {
  constructor(props) {
    super(props);

    this.tick = this.tick.bind(this);
    this.reset = this.reset.bind(this);

    this.state = {
      seconds: 0

    }
  }

  reset() {
    this.setState(state => ({
      seconds: 0,
    }));
  }

  tick() {
    this.setState(state => ({
      seconds: state.seconds + 1,
    }));
  }

  componentDidMount() {
    this.interval = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }



    render() {
      return (
        <div className="App">
            <h1> Authors </h1>
            <span>Seconds : {this.state.seconds} </span>
            <div>
            <Button onClick={this.tick}>Add 1</Button>{' '}
            <Button onClick={this.reset}>Reset</Button>{' '}
            </div>
            
        </div>

        
        
      );
    }
  }

  export default Authors;
