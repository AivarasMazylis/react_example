import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input} from 'reactstrap';
// import TodoItem from '../../components/TodoList/TodoItem';
import './TodoList.css';


class TodoList extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      items: [],
      text: ''
    }
  }
  handleSubmit(event) {
    event.preventDefault();
    
    if (!this.state.text.length) {
      return;
    }
    const newItem = {
      text: this.state.text,
      id : Date.now(),
    };

    

    this.setState(state => ({
      items: state.items.concat(newItem),
      text: '',
    }));
  }

  handleChange(e) {
    this.setState({text: e.target.value});
  }


    render() {
      return (
        <div className="App container">
            <h1>TodoList</h1>
            

            <Form onSubmit={this.handleSubmit}>
              <FormGroup>
               <Label htmlFor="new-todo">What needs to be done, bruh?</Label>
               <Input type="text" name="todo" id="new-todo"
                onChange={this.handleChange}
                value={this.state.text}
                placeholder="Go ahead, write..." />
              </FormGroup>
              <Button color="primary">Add</Button>
            </Form>
            {/* <TodoItem items={this.state.items}/> */}
            </div>
      );
    }
  }

  export default TodoList;