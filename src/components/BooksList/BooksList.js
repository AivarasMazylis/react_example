import React, { Component } from 'react';
import {
    Card,
    CardImg,
    CardText,
    CardBody,
    CardTitle,
    CardSubtitle,
    Button 
    }
    from 'reactstrap';
    import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class BooksList extends Component {
    render() {
        return (
    <div className="container">
        <div className = "row">
            {this.props.books.map((book, id) => (
             <Card key={id} className = "col-3 p-2">
                  <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
                  <CardBody>
                    <CardTitle>{book.pavadinimas}</CardTitle>
                    <CardSubtitle>{book.autorius}</CardSubtitle>
                    <CardText>Kategorija : {book.kategorija}
                     Book release : {book.suformuotaData} </CardText>
                     <Button onClick={() => this.props.deleteBook(book.id)}>
                         Delete {book.id}
                     </Button>
                  </CardBody>
                </Card>
                ))}
                    </div>
                <div className="row">
                <Form className="col-12 mt-4">
                    <FormGroup>
                         <Label for="FirstName">First Name</Label>
                            <Input type="text" name="Name" id="Name" placeholder="Enter First Name" />
                    </FormGroup>
                    <FormGroup>
                         <Label for="LastName">LastName</Label>
                            <Input type="text" name="Surname" id="Surname" placeholder="Enter Last Name" />
                    </FormGroup>
                    <FormGroup>
                         <Label for="FirstName">First Name</Label>
                            <Input type="text" name="BirthDate" id="BirthDate" placeholder="Enter Birth Date" />
                    </FormGroup>
                    <FormGroup tag="fieldset">
                        <legend>Author Alive</legend>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="yes" />{' '}
                                        Yes
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="no" />{' '}
                                        No
                                </Label>
                            </FormGroup>
                            </FormGroup>
                            <Button>Submit</Button>
                        </Form>
                    </div>
                </div>
            )
        }
    }

export default BooksList;