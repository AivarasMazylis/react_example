import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './NavBar.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';

 


class NavBar extends Component {
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
      render() {
        return (
          <div>
            <Navbar color="light" light expand="md" className="custom-navbar">
              <Link to="/" className="navbar-brand">Home</Link>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                  <Link to="/authors" className="navbar-brand">
                  Authors
                </Link>
                  </NavItem>
                  <NavItem>
                  <Link to="/todo-list" className="navbar-brand">
                  To do list
                </Link>
                  </NavItem>
                  <NavItem>
                  <Link to="/books" className="navbar-brand">
                  Books
                </Link>
                  </NavItem>
                  <NavItem>
                  <Link to="/exercise" className="navbar-brand">
                  Exercise
                </Link>
                  </NavItem>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      Author Options
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem>
                        Add Author
                      </DropdownItem>
                      <DropdownItem>
                        Edit Author
                      </DropdownItem>
                      <DropdownItem divider />
                      <DropdownItem>
                        Delete Author
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              </Collapse>
            </Navbar>
          </div>
        );
      }
} 

export default NavBar;