import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import NavBar from './components/NavBar';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";



import Books from './containers/Books';
import Authors from './containers/Authors';
import Home from './containers/Home';
import TodoList from './containers/TodoList';
import Exercise from './containers/Exercise';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <NavBar/>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/books" component={Books} />
              <Route path="/authors" component={Authors} />
              <Route path="/todo-list" component={TodoList} />
              <Route path="/exercise" component={Exercise} />
              <Route component={NoMatch} />
             </Switch>
          </div>
        </Router>
      );
   }
}

const NoMatch = ({ location }) => (
  <div>
    <h3>
      404 NOT FOUND
    </h3>
  </div>
);

export default App;
